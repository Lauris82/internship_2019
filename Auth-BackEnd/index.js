const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.set('port', (process.env.PORT || 5000));
var distDir = __dirname + "./routes";
app.use(express.static(distDir));

// Import Routes
const authRoute = require('./routes/auth');
const totpRoute = require('./routes/2fa');

dotenv.config();

// Connect to DB
mongoose.connect(
    process.env.DB_CONNECT,
    { useNewUrlParser: true },
    () => console.log('Connected to database!!!')
);

// Middleware
app.use(express.json());
// Route Middlewares
app.use('/api/user', authRoute);
app.use('/api/totp', totpRoute);

app.listen(app.get('port'), () => console.log('Server UP an running on port ', app.get('port')));