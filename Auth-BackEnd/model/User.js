const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 6,
        max: 20
    },
    email: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 1024
    },
    date: {
        type: Date,
        default: Date.now
    },
    tfa: {
        type: Boolean,
        default: false
    },
    tfa_secret: {
        type: String,
        required: true,
        max: 50
    }
});

module.exports = mongoose.model('User', userSchema);

