const User = require('../model/User');
const speakeasy = require('speakeasy');
const jwt = require('jsonwebtoken');
const router = require('express').Router();
const Nexmo = require('nexmo');
var HttpStatus = require('http-status-codes');

var email_tfa = require('./auth');

router.post('/setup', (req, res, next) => {
    var secret = speakeasy.generateSecret({ length: 20 });
    res.send({ "secret": secret.base32 });
});

router.post('/generate', async (req, res) => {
    console.log(`DEBUG: Generate TFA token`);
    var mail = email_tfa.tfa_email;

    // Check if the user is already in the database
    const tfa_secret = await User.findOne({email: mail}, 'tfa_secret', function(err, resdata){});
    
    var passcode = speakeasy.totp({
        secret: tfa_secret[1],
        encoding: "base32"
    });
    var remaining = (30 - Math.floor((new Date().getTime() / 1000.0 % 30)));

    const nexmo = new Nexmo({
        apiKey: '',
        apiSecret: '',
    });
        
    const from = 'Nexmo';
    const to = '';
    const text = 'Hi, your passcode is: ' + passcode + '\nThe remaining time is: ' + remaining + 's\n';
    
    nexmo.message.sendSms(from, to, text);

    res.send({
        "token": passcode,
        "remaining": remaining
    });
});

router.post('/validate', async (req, res) => {
    console.log(`DEBUG: Validate TFA`);
    var mail = email_tfa.tfa_email;

    // Check if the user is already in the database
    const tfa_secret = await User.findOne({email: mail}, 'tfa_secret', function(err, resdata){});
    
    let isVerified = speakeasy.totp.verify({
        secret: tfa_secret[1],
        encoding: "base32",
        token: req.body.token,
        window: 0
    });

    /*
    const user = await User.findOne({email: mail});
    const token = jwt.sign({_id: user._id}, user.tfa_secret);
    const _name = user.name;
    const _mail = user.email;

    const _token = JSON.stringify({token});

    _token,
    _name,
    _mail
*/

    if (isVerified) {
        return res.status(HttpStatus.ACCEPTED).send({
            "message": "Valid Auth Code. Access authorize"
        });
    }

    return res.status(HttpStatus.FORBIDDEN).send({
        "message": "Invalid Auth Code. Please verify the system Date and Time"
    });
});

router.post('/show', (req, res) => {
    res.send(email_tfa.tfa_email);
    console.log(email_tfa.tfa_email);
});


module.exports = router;