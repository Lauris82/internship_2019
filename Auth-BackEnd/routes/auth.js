const router = require('express').Router();
const User = require('../model/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const speakeasy = require('speakeasy');
const { registerValidation, loginValidation } = require('../validation');


// Register
router.post('/register', async (req, res) => {
    //Lets validate the data before saving a user
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // Check if the user is already in the database
    const emailExist = await User.findOne({email: req.body.email});
    if (emailExist) return res.status(400).send('Email already exist');

    // Hash the password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    // Generate tfa_secret
    var secret = speakeasy.generateSecret({ length: 20 });

    // Create a new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
        tfa_secret: secret.base32
    });
    try {
        const savedUser = await user.save();
        res.send({ user: user._id });
        
    } catch (err) {
        res.status(400).send(err);
    }
});

// Login
router.post('/login', async (req, res) => {
    //Lets validate the data before login in a user
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // Check if the email exists
    const user = await User.findOne({email: req.body.email});
    if (!user) return res.status(400).send('Email is not found!');

    // Check password
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(400).send('Invalid password!');

    module.exports.tfa_email = user.email;

    const token = jwt.sign({_id: user._id}, user.tfa_secret);
    const name = user.name;
    const mail = user.email;
    const dataS = JSON.stringify({name, mail, token});

    try {
        res.status(200).send(dataS);
    } catch (err) {
        res.status(400).send(err);
    }
    // Create and assign a token
    // const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET);
    // res.header('token').send(token);
});


module.exports = router;