const router = require('express').Router();
const checkToken = require('./verifyToken');

router.get('/', checkToken, (req, res) => {
    res.json({
        posts: {
            title: 'My post',
            description: 'Private post'
        }
    });
});

module.exports = router;