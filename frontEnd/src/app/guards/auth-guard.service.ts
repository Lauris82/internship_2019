import { Injectable } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private loginServ: LoginService, private router: Router) { }

  canActivate(): boolean {
    if (sessionStorage.getItem('tfa_totp')) {
      return true;
    }

    this.router.navigate(['/tfa']);
    return false;
  }
}
