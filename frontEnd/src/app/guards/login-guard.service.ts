import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardService implements CanActivate {

  constructor(private loginServ: LoginService, private router: Router) { }

  canActivate(): boolean {
    if (sessionStorage.getItem('user_info')) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}
