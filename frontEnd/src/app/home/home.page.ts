import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  name: any;
  mail: any;
  token: any;

  constructor(private router: Router) { }

  ngOnInit() {
    const minos = JSON.parse(sessionStorage.getItem('user_info'));
    this.name = minos.name;
    this.mail = minos.email;
    this.token = minos.token;
  }

  logout() {
    sessionStorage.removeItem('user_info');
    sessionStorage.removeItem('tfa_totp');
    this.router.navigate(['/login']);
  }

  get GetUsername(): string {
    return this.name;
  }

  get GetEmail(): string {
    return this.mail;
  }

  get GetToken(): string {
    return this.token;
  }

}
