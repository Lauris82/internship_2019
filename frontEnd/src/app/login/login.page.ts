import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  model: any = {};

  constructor(public loginServ: LoginService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    // console.log(this.model);
    this.loginServ.login(this.model).subscribe(() => {
      console.log('Logged in successfully');
    }, error => {
      console.log(error);
    }, () => {
      this.router.navigate(['/tfa']);
    });
  }

  loggedIn() {
    return this.loginServ.loggedIn();
  }

  gotoRegister() {
    this.router.navigate(['/register']);
  }

}
