import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  model: any = {};

  constructor(public loginServ: LoginService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    this.loginServ.register(this.model).subscribe(() => {
      console.log('Registration successful');
    }, error => {
      console.log('Registration unsuccessful');
    }, () => {
      this.router.navigate(['/login']);
    });
  }

  cancel() {
    this.router.navigate(['/login']);
  }

}
