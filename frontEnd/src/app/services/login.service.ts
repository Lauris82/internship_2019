import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // baseUrl = 'http://localhost:5000/api/user/';
  baseUrl = 'https://minosbackend.herokuapp.com/api/user/';
  jwtHelper = new JwtHelperService();
  decodedToken: any;

  constructor(private http: HttpClient) { }

  login(model: any) {
    return this.http.post(this.baseUrl + 'login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user && user.token) {
          const info = {
            name: user.name,
            email: user.mail,
            token: user.token
          };
          // const st = user.name + '\n' + user.mail + '\n' + user.token;
          // console.log(st);
          sessionStorage.setItem('user_info', JSON.stringify(info));
          // const minos = JSON.parse(sessionStorage.getItem('minos_token'));
          // console.log(minos.name + '\n' + minos.email + '\n' + minos.token);
        }
      })
    );
  }

  register(model: any) {
    return this.http.post(this.baseUrl + 'register', model);
  }

  loggedIn() {
    const token = sessionStorage.getItem('user_info');
    return !this.jwtHelper.isTokenExpired(token);
  }
}
