import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TfaService {
  // baseUrl = 'http://localhost:5000/api/totp/';
  baseUrl = 'https://minosbackend.herokuapp.com/api/totp/';

  constructor(private http: HttpClient) { }

  generateCode() {
    return this.http.post(this.baseUrl + 'generate', {}).pipe(
      map((response: any) => {
        const code = response;
        if (code) {
          console.log(code);
        }
      })
    );
  }

  validateCode(token: any) {

    return this.http.post(this.baseUrl + 'validate', { token }, { observe: 'response' });
    /*
    return this.http.post(this.baseUrl + 'validate', token).pipe(
      map((response: any) => {
        const isValide = response;
        if (isValide) {
          console.log(isValide);
        }
      })
    );
    */
  }

}
