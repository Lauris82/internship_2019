import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TfaPage } from './tfa.page';

describe('TfaPage', () => {
  let component: TfaPage;
  let fixture: ComponentFixture<TfaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TfaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TfaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
