import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TfaService } from '../services/tfa.service';

@Component({
  selector: 'app-tfa',
  templateUrl: './tfa.page.html',
  styleUrls: ['./tfa.page.scss'],
})
export class TfaPage implements OnInit {
  token: string;

  constructor(public tfaServ: TfaService, private router: Router) { }

  ngOnInit() {
  }

  generate() {
    this.tfaServ.generateCode().subscribe(() => {
      console.log('code generating......');
    });
  }

  verify() {
    this.tfaServ.validateCode(this.token).subscribe((data) => {
      console.log(data.status);
      if (data.status === 202) {
        sessionStorage.setItem('tfa_totp', 'SUCCESS');
        this.router.navigate(['/home']);
        console.log(data.body);
      } else {
        console.log(data.body);
      }
    });
  }

  cancel() {
    sessionStorage.removeItem('user_info');
    this.router.navigate(['/login']);
  }
}
